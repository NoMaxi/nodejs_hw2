const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();


const { port } = require('./config/server');
const db = require('./config/database');

const userRouter = require('./routers/userRouter');
const authRouter = require('./routers/authRouter');
const noteRouter = require('./routers/noteRouter');

mongoose.connect(`mongodb+srv://${db.userName}:${db.password}@${db.cluster}/${db.dbName}?retryWrites=true&w=majority`, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

app.use(express.static('./public'));

app.use(cors());
app.use(express.json());

app.use('/api', userRouter);
app.use('/api', authRouter);
app.use('/api', noteRouter);

const PORT = process.env.PORT || port;

app.listen(PORT, () => {
  console.log(`Server is running on ${port} port`);
});
