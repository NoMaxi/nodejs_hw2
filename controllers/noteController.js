const Note = require('../models/Note');

module.exports.getUserNotes = (req, res) => {
  Note.find({ userId: req.user._id })
    .then(notes => {
      if (!notes) {
        return res.status(400).json({ message: 'No notes found for current user' });
      }

      res.json({ notes });
    })
    .catch(err => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.addNote = (req, res) => {
  const { text } = req.body;
  if (!text) {
    return res.status(400).json({ message: 'No text in request body found' });
  }

  const note = new Note({ userId: req.user._id, text });
  note.save()
    .then(() => {
      res.json({ message: 'Success' });
    })
    .catch(err => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.getNoteById = (req, res) => {
  Note.findById(req.params.id).exec()
    .then(note => {
      if (!note) {
        return res.status(400).json({ message: 'Note not found' });
      }

      if (note.userId.toString() !== req.user._id) {
        return res.status(400).json({ message: 'You don\'t have access to this note' });
      }

      res.json({ note });
    })
    .catch(err => {
      if (!req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        return res.status(404).json({ message: 'Invalid note id' });
      }

      res.status(500).json({ message: err.message });
    });
};

module.exports.updateNote = (req, res) => {
  Note.findByIdAndUpdate(req.params.id, { text: req.body.text }).exec()
    .then(note => {
      if (!note) {
        return res.status(400).json({ message: 'Note not found' });
      }

      if (note.userId.toString() !== req.user._id) {
        return res.status(400).json({ message: 'You don\'t have access to this note' });
      }

      res.json({ message: 'Success' });
    })
    .catch(err => {
      if (!req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        return res.status(404).json({ message: 'Invalid note id' });
      }

      res.status(500).json({ message: err.message });
    });
};

module.exports.toggleNoteCompletion = (req, res) => {
  Note.findById(req.params.id).exec()
    .then(note => {
      if (!note) {
        return res.status(400).json({ message: 'Note not found' });
      }

      if (note.userId.toString() !== req.user._id) {
        return res.status(400).json({ message: 'You don\'t have access to this note' });
      }

      note.completed = !note.completed;
      return note.save();
    })
    .then(() => {
      res.json({ message: 'Success' });
    })
    .catch(err => {
      if (!req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        return res.status(404).json({ message: 'Invalid note id' });
      }

      res.status(500).json({ message: err.message });
    });
};

module.exports.deleteNote = (req, res) => {
  Note.findByIdAndDelete(req.params.id).exec()
    .then(note => {
      if (!note) {
        return res.status(400).json({ message: 'Note not found' });
      }

      if (note.userId.toString() !== req.user._id) {
        return res.status(400).json({ message: 'You don\'t have access to this note' });
      }

      res.json({ message: 'Success' });
    })
    .catch(err => {
      if (!req.params.id.match(/^[0-9a-fA-F]{24}$/)) {
        return res.status(404).json({ message: 'Invalid note id' });
      }

      res.status(500).json({ message: err.message });
    });
};
