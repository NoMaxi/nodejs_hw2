const bcrypt = require('bcryptjs');
const User = require('../models/User');
const { saltRounds } = require('../config/auth');

module.exports.getUser = (req, res) => {
  User.findById(req.user._id).exec()
    .then(user => {
      if (!user) {
        return res.status(400).json({ message: 'User not found' });
      }

      const { _id, username, createdDate } = user;
      res.json({ user: { _id, username, createdDate } });
    })
    .catch(err => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.changeUserPassword = (req, res) => {
  const { oldPassword, newPassword } = req.body;

  if (!oldPassword) {
    return res.status(400).json({
      message: 'No old password in request body found'
    });
  }

  if (!newPassword) {
    return res.status(400).json({
      message: 'No new password in request body found'
    });
  }

  bcrypt.hash(newPassword, saltRounds)
    .then(hashedNewPassword => {
      return User.findByIdAndUpdate(req.user._id, { password: hashedNewPassword }).exec()
    })
    .then(user => {
      if (!user) {
        return res.status(400).json({ message: 'User not found' });
      }

      return bcrypt.compare(oldPassword, user.password);
    })
    .then(arePasswordsEqual => {
      if (!arePasswordsEqual) {
        return res.status(400).json({ message: 'Incorrect old password' })
      }

      res.json({ message: 'Success' });
    })
    .catch(err => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.deleteUser = (req, res) => {
  User.findByIdAndDelete(req.user._id).exec()
    .then(user => {
      if (!user) {
        return res.status(400).json({ message: 'User not found' });
      }

      res.json({ message: 'Success' });
    })
    .catch(err => {
      res.status(500).json({ message: err.message });
    })
};
