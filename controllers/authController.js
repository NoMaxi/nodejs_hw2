const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/User');

const { secret, saltRounds } = require('../config/auth');

module.exports.register = (req, res) => {
  const { username, password } = req.body;
  if (!username) {
    return res.status(400).json({ message: 'No username in request body found' });
  }

  if (!password) {
    return res.status(400).json({ message: 'No password in request body found' });
  }

  bcrypt.hash(password, saltRounds)
    .then(hashedPassword => {
      const user = new User({ username, password: hashedPassword });
      return user.save();
    })
    .then(() => {
      res.json({ message: 'Success' });
    })
    .catch(err => {
      res.status(500).json({ message: err.message });
    });
};

module.exports.login = (req, res) => {
  const { username, password } = req.body;
  if (!username) {
    return res.status(400).json({ message: 'No username in request body found' });
  }

  if (!password) {
    return res.status(400).json({ message: 'No password in request body found' });
  }

  User.findOne({ username }).exec()
    .then(user => {
      if (!user) {
        return res.status(400).json({
          message: 'No user with such username found'
        });
      }
      return Promise.all([
        bcrypt.compare(password, user.password),
        Promise.resolve(user)
      ]);
    })
    .then(([arePasswordsEqual, user]) => {
      if (!arePasswordsEqual) {
        return res.status(400).json({ message: 'Incorrect password' })
      }
      res.json({ jwt_token: jwt.sign(JSON.stringify(user), secret) });
    })
    .catch(err => {
      res.status(500).json({ message: err.message });
    });
};
