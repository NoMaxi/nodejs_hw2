const express = require('express');
const router = express.Router();
const authMiddleware = require('../middlewares/authMiddleware');

const {
  getUser,
  changeUserPassword,
  deleteUser
} = require('../controllers/userController');

router.get('/users/me', authMiddleware, getUser);
router.patch('/users/me', authMiddleware, changeUserPassword);
router.delete('/users/me', authMiddleware, deleteUser);

module.exports = router;
