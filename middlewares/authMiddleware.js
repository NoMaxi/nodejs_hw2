const jwt = require('jsonwebtoken');
const { secret } = require('../config/auth');

module.exports = (req, res, next) => {
  const authHeader = req.headers['authorization'];

  if (!authHeader) {
    return res.status(401).json({ status: 'No authorization header found' });
  }

  const [, jwtToken] = authHeader.split(' ');

  try {
    req.user = jwt.verify(jwtToken, secret);
    next();
  } catch (err) {
    return res.status(401).json({ status: 'Invalid JWT' });
  }
};
